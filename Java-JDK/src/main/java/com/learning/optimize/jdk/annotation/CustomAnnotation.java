package com.learning.optimize.jdk.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ClassName: CustomAnnotation
 * Description: 自定义注解
 * Date: 2018/7/18 15:06 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Documented
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CustomAnnotation {

    /**
     * 字符串类型
     *
     * @return
     */
    String name() default "";

    /**
     * 布尔类型
     *
     * @return
     */
    boolean canShow() default false;


    /**
     * int 类型
     *
     * @return
     */
    int value() default 0;

    /**
     * 数组类型
     *
     * @return
     */
    String[] names() default "";

    /**
     * 枚举类型
     */
    enum Status {
        /**
         * 修复
         */
        FIXED,
        /**
         * 正常
         */
        NORMAL
    }

    /**
     * class类型
     */
    Class<?> clazz() default Void.class;


}
