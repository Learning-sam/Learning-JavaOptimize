package com.learning.optimize.jdk.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ClassName: GroupAnnotation
 * Description: 组合注解
 * Date: 2018/7/18 15:14 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface GroupAnnotation {
    int value() default 0;

    String name() default "";
}
