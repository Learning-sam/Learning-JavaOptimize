package com.learning.optimize.jdk.annotation.sql;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ClassName: Column
 * Description: 列注解
 * Date: 2018/7/18 15:06 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {

    /**
     * 对应数据库表的列名
     *
     * @return
     */
    String name() default "";

    /**
     * 列类型分配的长度，如varchar(30)的30
     *
     * @return
     */
    int value() default 16;

    /**
     * 列约束
     *
     * @return
     */
    Constraints constraint() default @Constraints;
}