package com.learning.optimize.jdk.annotation.sql;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * ClassName: Constraints
 * Description: 列约束
 * Date: 2018/7/18 15:06 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Constraints {
    /**
     * 判断是否作为主键约束
     */
    boolean primaryKey() default false;

    /**
     * 判断是否允许为null，默认不为 null
     *
     * @return
     */
    boolean allowNull() default false;

    /**
     * 判断是否唯一，默认不唯一
     *
     * @return
     */
    boolean unique() default false;
}