package com.learning.optimize.jdk.annotation.sql;

import lombok.Getter;
import lombok.Setter;

/**
 * ClassName: Person
 * Description: 数据库表实体
 * Date: 2018/7/18 15:06 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Table(name = "sys_person")
@Setter
@Getter
public class Person {
    /**
     * 主键ID
     */
    @Column(name = "ID", value = 50, constraint = @Constraints(primaryKey = true))
    private String id;

    /**
     * 姓名
     */
    @Column(name = "NAME", value = 30)
    private String name;

    /**
     * 年龄
     */
    @Column(name = "AGE")
    private int age;

    /**
     * 个人描述
     */
    @Column(name = "DESCRIPTION", value = 150, constraint = @Constraints(allowNull = true))
    private String description;


}