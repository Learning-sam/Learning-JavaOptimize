package com.learning.optimize.jdk.annotation.sql;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ClassName: Table
 * Description: 表注解
 * Date: 2018/7/18 15:06 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Table {
    /**
     * 表的名字
     *
     * @return
     */
    String name();
}