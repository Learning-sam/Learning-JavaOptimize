package com.learning.optimize.jdk.classloader;

import java.io.InputStream;

/**
 * ClassName: ClassLoaderLoadResource
 * Description: 利用ClassLoader 加载资源: 默认从根目录加载
 * Date: 2018/7/17 17:00 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class ClassLoaderLoadResource {

    public static void main(String[] args) {

        // 默认则是从 ClassPath 根下获取，path不能以 '/' 开头，最终是由 ClassLoader 获取资源
        InputStream ips2 = ClassLoaderLoadResource.class.getClassLoader().getResourceAsStream("config2.properties");

        // path 不以 '/' 开头，就是针对当前的类。是从此类所在的包下取资源
        // path 以 '/' 开头，则是从 ClassPath(Src根目录) 下获取资源
        InputStream ips3 = ClassLoaderLoadResource.class.getResourceAsStream("config2.properties");
    }

}
