package com.learning.optimize.jdk.classloader;

import java.util.Date;

/**
 * ClassName: CustomClassLoaderAttachment
 * Description: 被自定义类加载器 加载的类
 * Date: 2018/7/17 15:11 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class CustomClassLoaderAttachment extends Date {


    private String name;

    public CustomClassLoaderAttachment(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("CustomClassLoaderAttachment{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
