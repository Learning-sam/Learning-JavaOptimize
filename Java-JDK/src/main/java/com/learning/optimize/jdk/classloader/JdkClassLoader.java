package com.learning.optimize.jdk.classloader;

/**
 * JDK内置的ClassLoader
 *
 * @author SAM-SHO
 * <p>
 * 1. bootstrap class loader 最基本的 classloader,本地语言编写(c++)，使用他加载JDK的核心 class——rt.jar, 一般拿不到名字，是空值
 * 2. extesion class loader 扩展classloader，负责加载JDK的扩展类(jre/lib/ext下面的类)
 * 3. application class loader 负责我们自己定义的类,也叫系统class loader
 */
public class JdkClassLoader {

    public static void main(String[] args) {
//        test();
        test2();
    }


    /**
     * 类加载器
     */
    static void test() {

        //1. bootstrap class loader： null，拿不到 BootStrap 的名字
        System.out.println(String.class.getClassLoader());

        //2. extesion class loader 负责扩展包中的类(jre/lib/ext下面的类) sun.misc.Launcher$ExtClassLoader
        System.out.println(com.sun.crypto.provider.DESKeyFactory.class.getClassLoader().getClass().getName());

        //3. application class loader 负责自己写的类，也称为系统 classLoader。sun.misc.Launcher$AppClassLoader
        System.out.println(JdkClassLoader.class.getClassLoader().getClass().getName());

        // sun.misc.Launcher$AppClassLoader@14dad5dc
        System.out.println(ClassLoader.getSystemClassLoader());
        // sun.misc.Launcher$AppClassLoader@19821f
        System.out.println(ClassLoader.getSystemClassLoader().getClass().getName());

        // sun.misc.Launcher$AppClassLoader@14dad5dc
        System.out.println(JdkClassLoader.class.getClassLoader());
        // sun.misc.Launcher$AppClassLoader
        System.out.println(JdkClassLoader.class.getClassLoader().getClass().getName());

        /*
            输出：
            null
            sun.misc.Launcher$ExtClassLoader
            sun.misc.Launcher$AppClassLoader
            sun.misc.Launcher$AppClassLoader@14dad5dc
            sun.misc.Launcher$AppClassLoader
            sun.misc.Launcher$AppClassLoader@14dad5dc
            sun.misc.Launcher$AppClassLoader
        */
    }

    /**
     * 类加载器的委托机制
     */
    static void test2() {

        /*
         * classLoader之间的关系，不是继承
         * 加载过程：先找上一层(parent,但不是继承)classloader,如果已经被加载了，不会被再加载
         * 顺序应该为：AppClassLoader---> ExtClassLoader--->BootStrap
         */
        ClassLoader loader = JdkClassLoader.class.getClassLoader();
        while (loader != null) {
            System.out.println("=======" + loader.getClass().getName());
            loader = loader.getParent();
        }
        System.out.println("-------" + loader);

        /*
            输出:
           =======sun.misc.Launcher$AppClassLoader
           =======sun.misc.Launcher$ExtClassLoader
           -------null
         */
    }
}
