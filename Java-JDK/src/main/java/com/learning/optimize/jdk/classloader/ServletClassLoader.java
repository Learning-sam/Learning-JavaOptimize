package com.learning.optimize.jdk.classloader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * ClassName: ServletClassLoader
 * Description: Tomcat 实现的类加载器（Tomcat8.5）
 * Date: 2018/7/17 16:50 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@WebServlet(name = "ServletClassLoader", value = {"/servletClassLoader"})
public class ServletClassLoader extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        //系统加载器
        out.println("----" + ClassLoader.getSystemClassLoader().getClass().getName());


        //循环遍历加载器
        ClassLoader loader = this.getClass().getClassLoader();

        while (loader != null) {
            out.println(loader.getClass().getName() + "<br/>");
            loader = loader.getParent();
        }

        out.println(loader);


        out.flush();
        out.close();

    }

    /*
        sun.misc.Launcher$AppClassLoader org.apache.catalina.loader.ParallelWebappClassLoader
        java.net.URLClassLoader
        sun.misc.Launcher$AppClassLoader
        sun.misc.Launcher$ExtClassLoader
        null
    */
}
