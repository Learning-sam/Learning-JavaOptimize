package com.learning.optimize.jdk.clazz;

/**
 * ClassName: Instance
 * Description: instanceof、isInstance 测试
 * Date: 2018/7/23 10:49 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class Instance {

    public static void main(String[] args) {
//        test(new A());
        test(new B());
    }

    static void test(Object x) {
        print("Testing x of type " + x.getClass());
        print("x instanceof A " + (x instanceof A));
        print("x instanceof B " + (x instanceof B));
        print("A.isInstance(x) " + A.class.isInstance(x));
        print("B.isInstance(x) " + B.class.isInstance(x));
        print("x.getClass() == A.class " + (x.getClass() == A.class));
        print("x.getClass() == B.class " + (x.getClass() == B.class));
        print("x.getClass().equals(A.class)) " + (x.getClass().equals(A.class)));
        print("x.getClass().equals(B.class)) " + (x.getClass().equals(B.class)));
    }

    private static void print(Object obj) {
        System.out.println(obj);
    }

}

/*
* Testing x of type class com.learning.optimize.jdk.reflect.clazz.A
    x instanceof A true
    x instanceof B false // 父类不一定是子类的某个类型
    A.isInstance(x) true
    B.isInstance(x) false
    x.getClass() == A.class true
    x.getClass() == B.class false
    x.getClass().equals(A.class)) true
    x.getClass().equals(B.class)) false
    ------------------------------------------------
    Testing x of type class com.learning.optimize.jdk.reflect.clazz.B
    x instanceof A true
    x instanceof B true
    A.isInstance(x) true
    B.isInstance(x) true
    x.getClass() == A.class false
    x.getClass() == B.class true
    x.getClass().equals(A.class)) false
    x.getClass().equals(B.class)) true

*
* */


class A {
}

class B extends A {
}
