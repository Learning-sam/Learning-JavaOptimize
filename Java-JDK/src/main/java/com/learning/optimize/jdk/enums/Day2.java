package com.learning.optimize.jdk.enums;

/**
 * ClassName: Day2
 * Description: 添加方法与自定义构造函数
 * Date: 2018/7/18 14:18 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
enum Day2 {
    MONDAY("星期一"),
    TUESDAY("星期二"),
    WEDNESDAY("星期三"),
    THURSDAY("星期四"),
    FRIDAY("星期五"),
    SATURDAY("星期六"),
    /**
     * 最后需要用分号结束
     */
    SUNDAY("星期日");

    private String desc;

    private Day2(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}