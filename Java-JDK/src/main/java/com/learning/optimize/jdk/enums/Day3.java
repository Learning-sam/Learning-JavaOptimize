package com.learning.optimize.jdk.enums;

/**
 * ClassName: Day2
 * Description: 添加方法与自定义构造函数
 * Date: 2018/7/18 14:18 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
enum Day3 {
    /**
     * 星期一
     */
    MONDAY("星期一") {
        @Override
        public String getNextDay() {
            return Day3.TUESDAY.desc;
        }
    },
    TUESDAY("星期二") {
        @Override
        public String getNextDay() {
            return Day3.WEDNESDAY.desc;
        }
    },
    WEDNESDAY("星期三") {
        @Override
        public String getNextDay() {
            return Day3.THURSDAY.desc;
        }
    },
    THURSDAY("星期四") {
        @Override
        public String getNextDay() {
            return Day3.FRIDAY.desc;
        }
    },
    FRIDAY("星期五") {
        @Override
        public String getNextDay() {
            return Day3.SATURDAY.desc;
        }
    },
    SATURDAY("星期六") {
        @Override
        public String getNextDay() {
            return Day3.SUNDAY.desc;
        }
    },
    /**
     * 最后需要用分号结束
     */
    SUNDAY("星期日") {
        @Override
        public String getNextDay() {
            return Day3.MONDAY.desc;
        }
    };

    private String desc;

    Day3(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * 定义抽象方法
     *
     * @return
     */
    public abstract String getNextDay();
}