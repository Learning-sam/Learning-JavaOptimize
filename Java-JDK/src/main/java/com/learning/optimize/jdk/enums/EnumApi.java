package com.learning.optimize.jdk.enums;

import java.util.Arrays;

/**
 * ClassName: EnumApi
 * Description: 枚举
 * Date: 2018/7/18 15:40 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class EnumApi {

    public static void main(String[] args) {
//        test();
//        test2();
//        test3();
        test4();
    }

    static void test() {
        //创建枚举数组
        Day[] days = new Day[]{Day.MONDAY, Day.TUESDAY, Day.WEDNESDAY,
                Day.THURSDAY, Day.FRIDAY, Day.SATURDAY, Day.SUNDAY};

        // ordinal
        for (int i = 0; i < days.length; i++) {
            System.out.println("day[" + i + "].ordinal():" + days[i].ordinal());
        }

        System.out.println("-------------------------------------");
        // compareTo
        //通过compareTo方法比较,实际上其内部是通过ordinal()值比较的
        System.out.println("days[0].compareTo(days[1]):" + days[0].compareTo(days[1]));
        System.out.println("days[0].compareTo(days[1]):" + days[0].compareTo(days[2]));

        // 获取该枚举对象的Class对象引用,当然也可以通过getClass方法
        // 返回与此枚举常量的枚举类型相对应的 Class 对象
        Class<?> clazz = days[0].getDeclaringClass();
        System.out.println("clazz:" + clazz);

        System.out.println("-------------------------------------");

        //name()
        System.out.println("days[0].name():" + days[0].name());
        System.out.println("days[1].name():" + days[1].name());
        System.out.println("days[2].name():" + days[2].name());
        System.out.println("days[3].name():" + days[3].name());

        System.out.println("-------------------------------------");

        // toString
        System.out.println("days[0].toString():" + days[0].toString());
        System.out.println("days[1].toString():" + days[1].toString());
        System.out.println("days[2].toString():" + days[2].toString());
        System.out.println("days[3].toString():" + days[3].toString());

        System.out.println("-------------------------------------");

        // valueOf
        Day d = Enum.valueOf(Day.class, days[0].name());
        Day d2 = Day.valueOf(Day.class, days[0].name());
        System.out.println("d:" + d);
        System.out.println("d2:" + d2);

        // valueOf
        Day day1 = Day.valueOf(days[0].name());
        System.out.println(day1);

        // values
        Day[] values = Day.values();
        String s = Arrays.toString(values);
        System.out.println(s);
    }

    /**
     * 枚举的反射
     */
    static void test2() {
        Day day = Day.MONDAY;
        // 向上转型
        Enum monday = Day.MONDAY;

        //无法调用,没有此方法
        //monday.values();

        //获取class对象引用
        Class<?> clazz = monday.getDeclaringClass();
        if (clazz.isEnum()) {
            Day[] dsz = (Day[]) clazz.getEnumConstants();
            System.out.println("dsz:" + Arrays.toString(dsz));
        }

        // dsz:[MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY]
    }

    /**
     * 向enum类添加方法与自定义构造函数
     */
    static void test3() {
        for (Day2 day : Day2.values()) {
            System.out.println("name:" + day.name() + ",desc:" + day.getDesc());
        }

        /*
        name:MONDAY,desc:星期一
        name:TUESDAY,desc:星期二
        name:WEDNESDAY,desc:星期三
        name:THURSDAY,desc:星期四
        name:FRIDAY,desc:星期五
        name:SATURDAY,desc:星期六
        name:SUNDAY,desc:星期日
        * */
    }

    /**
     * enum类中定义抽象方法
     */
    static void test4() {
        Day3 monday = Day3.MONDAY;
        String nextDay = monday.getNextDay();
        System.out.println(nextDay);

        System.out.println(Day3.FRIDAY.getNextDay());
    }

    /**
     * enum类与接口
     */
    static void test5() {
        Food food = Food.Appetizer.SALAD;
        food = Food.MainCourse.LASAGNE;
        food = Food.Dessert.GELATO;
        food = Food.Coffee.CAPPUCCINO;
    }

    /**
     * 枚举与switch
     */
    static void test6(Day2 day) {
        switch (day) {
            case MONDAY:
                System.out.println(day.getDesc());
                break;
            case TUESDAY:
                System.out.println(day.getDesc());
                break;
            case WEDNESDAY:
                System.out.println(day.getDesc());
                break;
            case THURSDAY:
                System.out.println(day.getDesc());
                break;
            case FRIDAY:
                System.out.println(day.getDesc());
                break;
            case SATURDAY:
                System.out.println(day.getDesc());
                break;
            case SUNDAY:
                System.out.println(day.getDesc());
                break;
            default:
                System.out.println(day.getDesc());
        }
    }



}
