package com.learning.optimize.jdk.enums;

/**
 * ClassName: Meal
 * Description: 由于Java单继承的原因，enum类并不能再继承其它类，但并不妨碍它实现接口，因此enum类同样是可以实现多接口的
 * Date: 2018/7/18 15:40 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public enum Meal {
    /**
     * appetizer(开胃菜
     */
    APPETIZER(Food.Appetizer.class),
    /**
     * mainCourse(主菜)
     */
    MAINCOURSE(Food.MainCourse.class),
    /**
     * dessert(点心)
     */
    DESSERT(Food.Dessert.class),
    /**
     * Coffee
     */
    COFFEE(Food.Coffee.class);

    private Food[] values;

    private Meal(Class<? extends Food> kind) {
        //通过class对象获取枚举实例
        values = kind.getEnumConstants();
    }
} 