package com.learning.optimize.jdk.enums.singleton;

/**
 * ClassName: SingletonDoubleCheck
 * Description: 双重检查锁
 * 1、synchronized 保证线程安全
 * 2、synchronized 代码块，兼顾效率
 * 3、volatile（主内存空间）保证双重校验的可行性
 * 4、缺点：volatile 禁止指令重排序优化
 * Date: 2018/7/18 14:18 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class SingletonDoubleCheck {
    private static volatile SingletonDoubleCheck singleton = null;

    private SingletonDoubleCheck() {
    }

    public static SingletonDoubleCheck getSingleton() {
        if (singleton == null) {
            synchronized (SingletonDoubleCheck.class) {
                // volatile 关键字： 保证进行两次null检查
                if (singleton == null) {
                    singleton = new SingletonDoubleCheck();
                }
            }
        }
        return singleton;
    }
}