package com.learning.optimize.jdk.enums.singleton;

/**
 * ClassName: SingletonEnum
 * Description: 枚举单例
 * 1、使用简单 SingletonEnum.INSTANCE
 * 2、规避了 序列化与反序列化、反射 的问题
 * <p>
 * Date: 2018/7/18 14:18 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public enum SingletonEnum {
    /**
     * 单例对象
     */
    INSTANCE;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}