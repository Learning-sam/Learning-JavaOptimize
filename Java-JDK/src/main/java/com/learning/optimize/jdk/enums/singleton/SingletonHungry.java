package com.learning.optimize.jdk.enums.singleton;

/**
 * ClassName: SingletonHungry
 * Description: 饿汉式
 * 1、写法简单，基于 ClassLoader 机制避免了多线程的同步问题
 * 2、无法做到延迟创建对象，事实上如果该单例类涉及资源较多，创建比较耗时间时，我们更希望它可以尽可能地延迟加载，从而减小初始化的负载
 * Date: 2018/7/18 14:18 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class SingletonHungry {

    private static SingletonHungry instance = new SingletonHungry();

    private SingletonHungry() {
    }

    public static SingletonHungry getInstance() {
        return instance;
    }
}