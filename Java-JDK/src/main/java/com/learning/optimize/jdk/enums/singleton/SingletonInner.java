package com.learning.optimize.jdk.enums.singleton;

/**
 * ClassName: SingletonInner
 * Description: 静态内部类。
 * 1、把 Singleton 实例放到一个静态内部类中，这样可以避免了静态实例在 Singleton 类的加载阶段就创建对象
 * 2、静态变量初始化是在 SingletonInner 类初始化时触发的，并且由于静态内部类只会被加载一次，所以这种写法也是线程安全的
 * 3、一般大部分都是用这种方式
 * <p>
 * Date: 2018/7/18 14:18 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class SingletonInner {


    private SingletonInner() {
    }

    public static SingletonInner getSingleton() {
        return Holder.singleton;
    }

    /**
     * 静态内部类
     */
    private static class Holder {
        private static SingletonInner singleton = new SingletonInner();
    }
}