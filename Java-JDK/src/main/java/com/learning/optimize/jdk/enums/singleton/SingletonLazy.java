package com.learning.optimize.jdk.enums.singleton;

/**
 * ClassName: SingletonLazy
 * Description: 懒汉式单例模式（适合多线程安全）
 * 1、多线程需要使用 synchronized， 多线程中很好的工作避免同步问题。在单线程的情景下，完全可以去掉synchronized
 * 2、具备lazy loading机制
 * 3、由于synchronized 锁定整个方法，效率很低
 * Date: 2018/7/18 14:18 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class SingletonLazy {

    private static volatile SingletonLazy instance;

    private SingletonLazy() {
    }

    public static synchronized SingletonLazy getInstance() {
        if (instance == null) {
            instance = new SingletonLazy();
        }
        return instance;
    }
}