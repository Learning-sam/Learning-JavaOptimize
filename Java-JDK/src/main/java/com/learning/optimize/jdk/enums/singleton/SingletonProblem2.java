package com.learning.optimize.jdk.enums.singleton;

/**
 * ClassName: SingletonProblem
 * Description: 单例的问题
 * 1、序列化可能会破坏单例模式，比较每次反序列化一个序列化的对象实例时都会创建一个新的实例
 * 2、使用反射强行调用私有构造器，解决方式可以修改构造器，让它在创建第二个实例的时候抛异常
 * <p>
 * Date: 2018/7/18 14:18 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class SingletonProblem2 {

    public static SingletonProblem2 INSTANCE = new SingletonProblem2();

    private static volatile boolean flag = true;

    protected SingletonProblem2() {
        if (flag) {
            flag = false;
        } else {
            throw new RuntimeException("The instance already exists");
        }
    }


}   