package com.learning.optimize.jdk.genericity;

/**
 * ClassName: GenericityClazz
 * Description: 泛型类、泛型方法
 * Date: 2018/7/25 11:12 【需求编号】
 * <p>
 * http://www.importnew.com/24029.html
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class GenericityClazz<T> {

    private T t;

    public void set(T t) {
        this.t = t;
    }

    public T get() {
        return t;
    }

    /**
     * 编译报错：泛型类中的静态方法不能用泛型
     * 原因在于：静态方法不需要创建对象就可以使用，而泛型类却规定了对象的泛型
     * @param t
     */
//    public static void staticSet(T t) {}

    /**
     * 静态泛型方法的T 与 类T，不是同一个
     *
     * @param <T>
     * @return
     */
    public static <T> T staticSet(T t) {
        return null;
    }

}

/**
 * 一般的类
 */
class GeneralClazz {

    private String object;

    public void set(String object) {
        this.object = object;
    }

    public String get() {
        return object;
    }
}
