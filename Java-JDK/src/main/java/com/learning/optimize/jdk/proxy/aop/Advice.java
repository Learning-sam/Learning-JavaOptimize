package com.learning.optimize.jdk.proxy.aop;


import java.lang.reflect.Method;

/**
 * ClassName: Advice
 * Description: 切面的接口，简单实现
 * Date: 2018/7/19 17:22 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public interface Advice {

    /**
     * 方法前调用
     *
     * @param method
     */
    void before(Method method);

    /**
     * 方法后调用
     *
     * @param method
     */
    void after(Method method);

    /**
     * 异常
     * @param method
     */
    void afterThrow(Method method);

    /**
     * Finally 处理
     * @param method
     */
    void afterFinally(Method method);
}

class CustomAdvice implements Advice {
    private long beginTime = 0;

    @Override
    public void before(Method method) {
        System.out.println("~~~ 在方法调用之前 ~~~");
        beginTime = System.currentTimeMillis();
    }

    @Override
    public void after(Method method) {
        System.out.println("~~~ 方法已经调用结束啦 ~~~");
        long endTime = System.currentTimeMillis();
        System.out.println(method.getName() + " 方法一共运行的时间为 " + (endTime - beginTime));
    }

    @Override
    public void afterThrow(Method method) {

    }

    @Override
    public void afterFinally(Method method) {

    }
}
