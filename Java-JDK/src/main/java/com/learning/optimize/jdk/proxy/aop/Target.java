package com.learning.optimize.jdk.proxy.aop;


/**
 * ClassName: Target
 * Description: 被代理对象接口。Proxy 要求需要实现接口
 * Date: 2018/7/25 9:27 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public interface Target {

    /**
     * 方法
     */
    void first();
}

class CustomTarget implements Target {

    @Override
    public void first() {
        System.out.println("方法 first 被调用了");
    }

}
