package com.learning.optimize.jdk.proxy.cglib;

/**
 * ClassName: CglibTarget
 * Description: CglibTarget 实现的被代理目标对象，可以不实现接口
 * Date: 2018/7/25 10:24 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class CglibTarget {

    void first() {
        System.out.println("first 方法被调用");
    }
}
