package com.learning.optimize.jdk.reflect.jdbc;

import java.util.List;


/**
 * ClassName: GenericDao
 * Description: 基础接口
 * Date: 2018/7/18 14:19 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public interface GenericDao<T> {
    /**
     * 获取所有接口
     *
     * @return
     * @throws Exception
     */
    List<T> getALL() throws Exception;

}