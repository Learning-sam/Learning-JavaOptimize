package com.learning.optimize.jdk.reflect.jdbc;

import java.util.List;

/**
 * ClassName: JdbcReflect
 * Description: Jdbc 反射测试
 * Date: 2018/7/18 14:37 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class JdbcReflect {

    public static void main(String[] args) throws Exception {
        UserDaoImpl userDao = new UserDaoImpl();
        List<User> users = userDao.getALL();
        System.out.println(users);
    }
}
