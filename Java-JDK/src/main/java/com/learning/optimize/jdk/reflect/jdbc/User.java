package com.learning.optimize.jdk.reflect.jdbc;

import lombok.Getter;
import lombok.Setter;

/**
 * ClassName: User
 * Description:
 * Date: 2018/7/18 14:41 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Setter
@Getter
public class User {

}
