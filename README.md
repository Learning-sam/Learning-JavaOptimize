# Learning-JavaOptimize JAVA 进阶

#### 项目介绍
Java 进阶学习，包括JDK、集合、JVM进阶学习，需要一定的 Java 基础

#### 博客地址
1. 私人博客：https://sam-blog.gitee.io/
2. CSDN：https://blog.csdn.net/u012228718

#### 前提
1. 插件安装：lombok

#### 软件架构
1. JDK8
2. Servlet 3.x
3. guava 23
4. cglib 3.1


#### 使用说明

1. Java-JDK JDK代码层进阶
- 类加载器：`com.learning.optimize.jdk.classloader`
- 反射：`com.learning.optimize.jdk.reflect`
- 枚举：`com.learning.optimize.jdk.enums`
- 注解：`com.learning.optimize.jdk.annotation`
- 泛型：`com.learning.optimize.jdk.genericity`
- 动态代理：`com.learning.optimize.jdk.proxy`

2. Java-JVM JVM虚拟机进阶
- JVM 内存
- JVM 调优

3. Java-Collection JDK集合进阶
- 数据结构
- 主要集合：List、Set、Map